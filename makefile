compiler=gcc
opt=-O3
debugflag=-ggdb3 -Wall
debugflag=
#gmon=-pg
gmon=
src=./src/
doc=./doc/

all: help_funcs.o structs.o methods.o parse.o arguments.o main.o main clean #docs

help_funcs.o : $(src)parse/help_funcs.c $(src)structs/structs.c $(src)cli/hilfe
	$(compiler) $(gmon) $(debugflag) $(opt)  -c -o $(src)$@ $<

structs.o : $(src)structs/structs.c
	$(compiler) $(gmon) $(debugflag) $(opt)  -c -o $(src)$@ $<

methods.o : $(src)structs/methods.c $(src)structs/methods.h
	$(compiler) $(gmon) $(debugflag) $(opt)  -c -o $(src)$@ $<

parse.o : $(src)parse/parse.c $(src)parse/parse.h $(src)parse/help_funcs.h
	$(compiler) $(gmon) $(debugflag) $(opt)  -c -o $(src)$@ $<

arguments.o : $(src)cli/arguments.c $(src)cli/arguments.h $(src)structs/structs.c $(src)cli/help.txt
	$(compiler) $(gmon) $(debugflag) $(opt)  -c -o $(src)$@ $<

main.o : $(src)main.c $(src)parse/parse.h $(src)structs/structs.c
	$(compiler) $(gmon) $(debugflag) $(opt)  -c -o $(src)$@ $<

main : main.o parse.o help_funcs.o structs.o
	$(compiler) $(gmon)  $(debugflag) $(opt)  -o aloes $(src)structs.o $(src)methods.o $(src)help_funcs.o $(src)arguments.o $(src)parse.o $(src)main.o

clean :
	-rm $(src)*.o

#docs :
#	if [ ! -f "./docs.pdf" ] || [ ! -f "./docs.html" ]; then\
#		doxygen $(doc)doxconfig;\
#		make -s -C $(doc)latex/;\
#		ln -s $(doc)latex/refman.pdf ./docs.pdf;\
#		ln -s $(doc)html/index.html ./docs.html;\
#	fi
