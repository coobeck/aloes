[Installation process]<br>
Use <b>make</b> in directory containing this <b>README</b> file<br>
to build the program and generate documentation.


[Usage description]<br>
./aloes -h<br>
./aloes -f FILE [OPTION]


Flags and options:<br>
-h              Print help<br>
-l              Output all available gene ontology namespaces<br>
-n NAMESPACE    Output terms' ids and corresponding levels of NAMESPACE
