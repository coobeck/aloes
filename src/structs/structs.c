#ifndef STRUCTS_H
#define STRUCTS_H

#include <stdlib.h>

/** @struct term
 * @brief Struct for each term found in input file
 * @var term::id
 * Id of term in question.
 * @var term::no_parents
 * Number of term's parents.
 * @var term::name
 * Name of term.
 * @var term::nmspc
 * Namespace name to which the term belongs
 * @var term::parents
 * Array of term's parents' ids
 */
typedef struct term
{
  int id;
  int no_parents;
//  char *name;
  char *nmspc;
  int *parents;
} term;

/** @struct trmarr
 * @brief Struct "array of terms".
 * @var trmarr::terms
 * Pointer to array of all terms.
 * @var trmarr::no_terms
 * Number off term terms.
 * @var trmarr::namespaces
 * Pointer to array of all namespaces provided in input file.
 * @var trmarr::count
 * Number of available namespaces.
 */
typedef struct trmarr
{
  term *terms;
  int no_terms;
  char **namespaces;
  int count;
} trmarr;

/** @struct namespace_branch
 * @brief Struct of a single namespace (trmarr wrapper)
 * @var namespace_branch::n
 * Number of terms in belonging to namespace_branch::name
 * @var namespace_branch::name
 * Name of a namespace
 * @var namespace_branch::terms
 * Pointer to array of all terms belonging to namespace_branch::name
 */
typedef struct namespace_branch
{
  int n;
  char *name;
  term *terms;
} namespace_branch;


/** @struct namespace_tree
 * @brief Node of a DA graph
 * @var namespace_tree::curr_term
 * Pointer to \ref term of the node.
 * @var namespace_tree::level
 * Level of namespace_tree::curr_term relative to branch node.
 * @var namespace_tree::parents
 * Pointer to list of namespace_tree::curr_term 's parents of type \ref namespace_tree.
 */
typedef struct namespace_tree
{
  term *curr_term;
  int level;
  struct namespace_tree **parents;
} namespace_tree;

#endif
