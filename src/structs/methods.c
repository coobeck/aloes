#include "methods.h"

namespace_branch *
initialise_branches (trmarr * term_array)
{
    //allocate space for branches of all available namespaces
  namespace_branch *branches =
    (namespace_branch *) calloc (term_array->count, sizeof (namespace_branch));

  /* iterate through available namespaces, filling their
  *names, and initializing other parameters */
  for (int i = 0; i < term_array->count; ++i)
    {
      branches[i].name = term_array->namespaces[i];
      branches[i].n = 0;
      branches[i].terms = NULL;
    }


  /* iterate through all terms */
  for (int i = 0; i < term_array->no_terms; ++i)
    {
        /* iterate through branches */
      for (int j = 0; j < term_array->count; ++j)
	{
        //check, if branch's name exists
	  if (branches[j].name != NULL)
	    {
            /* if term's name is the same as branch's name -> add 
             * the term to branch */
	      if (strcmp (term_array->terms[i].nmspc, branches[j].name) == 0)
		{
		  branches[j].n++;
		  branches[j].terms =
		    (term *) realloc (branches[j].terms,
				      branches[j].n * sizeof (term));
		  branches[j].terms[branches[j].n - 1] = term_array->terms[i];
		}
	    }
	}
    }
  return branches;
}

int
bin_find (int parent_id, namespace_branch array, int low, int high)
    /* helper function, simple implementation of binary search */
{
  int avg = (low + high) / 2;
  if (parent_id < array.terms[avg].id)
    {
      return bin_find (parent_id, array, low, avg);
    }
  if (parent_id > array.terms[avg].id)
    {
      return bin_find (parent_id, array, avg + 1, high);
    }
  return avg;
}

int *
find_parents (term * curr_term, namespace_branch branch, int *parents)
    /* helper function for listing term's parents' ids */
{
    /* iterate over number of term's parents */
  for (int j = 0; j < curr_term->no_parents; j++)
    {
        /* find term's parent in branch */
      parents[j] = bin_find (curr_term->parents[j], branch, 0, branch.n);
    }
  return parents;
}

int
level (namespace_tree * tree, int val)
    /* the most important function in the program,
     * calls itself until it find's namespace branch,
     * incrementing value of level (val) */
{
    //increment level
  ++val;

  //check if branch
//  if (strcmp (tree->curr_term->name, tree->curr_term->nmspc) == 0)
    if (!tree->curr_term->no_parents)
    {
      return val;
    }
  //create a list of term's parents
  int levels[tree->curr_term->no_parents];
  //call "level" function on parents
  for (int i = 0; i < tree->curr_term->no_parents; ++i)
    {
      levels[i] = level (tree->parents[i], val);
    }
  //choose which parent's level is the highest
  for (int i = 0; i < tree->curr_term->no_parents; ++i)
    {
      val = levels[i] > val ? levels[i] : val;
    }
  //return level
  return val;
}


namespace_tree **
namespace_insert (namespace_tree ** process, namespace_branch branch)
{
    //avoid clutter
  int no_terms = branch.n;

  //iterate over all namespace_branch's terms
  for (int n = 0; n < no_terms; ++n)
    {
        //allocate space for namespace_tree's term
      process[n] = (namespace_tree *) malloc (sizeof (namespace_tree));
      //make sure to pass a pointer
      process[n]->curr_term = &branch.terms[n];
      //alocate space for term's parents
      process[n]->parents =
	(namespace_tree **) calloc (process[n]->curr_term->no_parents,
				    sizeof (namespace_tree *));
    }

  //iterate over all namespace_branch's terms
  for (int i = 0; i < no_terms; ++i)
    {
      int *parents = (int *) calloc (branch.n, sizeof (int));
      //find all term's parents
      parents = find_parents (process[i]->curr_term, branch, parents);
      //pass parents to the term
      for (int j = 0; j < branch.terms[i].no_parents; ++j)
	{
	  int parent_index = parents[j];
	  process[i]->parents[j] = process[parent_index];
	}
      free (parents);
    }

  //get term's level
  for (int n = 0; n < no_terms; ++n)
    {
      process[n]->level = level (process[n], 0) - 1;
    }
  return process;
}

void
//free memory part 1
freetree (trmarr * terms, namespace_branch * branches, namespace_tree ** tree, int choice) {
  for (int t = 0; t < branches[choice].n; ++t)
    {
      free (tree[t]->parents);
    }
  for (int t = 0; t < branches->n; ++t)
    {
      free (tree[t]);
    }
  free (tree);
}

void
//free memory part 2
freenmspc (trmarr * terms, namespace_branch * branches)
{
  free (terms->namespaces);
  for (int no = 0; no < terms->no_terms; ++no)
    {
      free (terms->terms[no].parents);
     // free (terms->terms[no].name);
      free (terms->terms[no].nmspc);
    }
  free (terms->terms);
  for (int j = 0; j < terms->count; ++j)
    {
        free(branches[j].name);
      free (branches[j].terms);

    }
  free (terms);
  free (branches);
}
