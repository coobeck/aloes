#include "./structs.c"
#include <string.h>

#ifndef METHODS_H
#define METHODS_H

/**
 * @brief Initialise structure \ref namespace_branch
 * @param[term_array] Pointer to the initialised structure \ref trmarr
 * with pointer to parsed terms
 * @return pointer to newly initiased structure \ref namespace_branch
 */
namespace_branch *initialise_branches (trmarr * term_array);

/**
 * @brief Initialise first encountered node of type
 * \ref namespace_tree belonging to the namespace specified in
 * in branch argument.
 * @param[process] Pointer to the structure to be initiased.
 * @param[branch] Structure of specified namespace name.
 * @return Pointer to initiased structure \ref namespace_tree
 */
namespace_tree **namespace_insert (namespace_tree ** process,
				   namespace_branch branch);

/**
 * @brief Free memory allocated to structure of type \ref namespace_tree
 * @param[terms] Pointer to initiased structure of type \ref trmarr
 * @param[branches] Pointer to array of initialised branches
 * @param[tree] Pointer to pointer of initialised structure \ref namespace_tree
 * @param[choice] Index of chosen namespace
 */
void freetree (trmarr * terms, namespace_branch * branches,
	       namespace_tree ** tree, int choice);

/**
 * @brief Free memory allocated to the passed arguments
 * @param[terms] Pointer to initiased structure of type \ref trmarr
 * @param[branches] Pointer to array of initialised branches
 */
void freenmspc (trmarr * terms, namespace_branch * branches);

#endif
