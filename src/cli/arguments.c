#include "./arguments.h"
#include "./hilfe"
void
print_all (trmarr * terms_array)
{
    /* iterate through all available namespaces */
  for (int i = 0; i < terms_array->count; ++i) 
    {
      printf ("%s\n", terms_array->namespaces[i]);
    }
}

int
choose_nmspc (namespace_branch * branches, trmarr * arr, char *nmspc)
{
    /* Get index of chosen namespace */
  for (int i = 0; i < arr->count; ++i)
    {
      if (strcmp (nmspc, arr->namespaces[i]) == 0)
	{
	  return i;
	}
    }
  printf ("Ontology not found, probably does not exist in file\n");
  return -1;
}

void
help ()
    /* Print short usage description */
{
    for (int i = 0; i < __help_txt_len; ++i) {
        printf("%c", __help_txt[i]);
    }
}

int levels(int argc, char **argv) {
        /*
    Check, if any call arguments were provided.
    If not, print short usage description
    */
  if (argc == 1)
    {
      help();
      return 1;
    }

  int choice;

  //Set pointers to basic structures
  trmarr *term_array;
  namespace_branch *branches;

  //Flag, describing what namespace is to be used
  int option;
  //Loop for parsing call arguments
	int opened = 0; //was file opened
  while ((option = getopt(argc, argv, "lf:hn:")) != -1)
    {
      switch (option)
	{

	//Flag "help"
	case 'h':
	{
	    help();
	break;
	}
      //Option with file location to process
	case 'f':
	  /* Process input file */
	{
	  //check if file exists
	if (access(optarg, F_OK) != 0) {
	    printf("File doesn't exist\n");
	return 1;
	}
	    FILE * inputfile = fopen(optarg, "r");

	    term_array = parse(inputfile);
        if (!(term_array->count && term_array->no_terms)) {
            printf("File does not contain any terms and/or ontologies\n");
            break;
            }
	    fclose(inputfile);
	    branches = initialise_branches(term_array);
	opened = 1;

	break;
	}
      //Flag "print all namespaces"
	case 'l':
	{
	if (!opened) {
	printf("Specify file (-f option), it also has to be run as the first opstion\n"); //change to flags
    break;
	}
	    print_all(term_array);
	break;
	}
      /* Option output to stdin chosen namespace's
       * terms' ids and corresponding levels */
	case 'n':
	{
	if (!opened) {
	printf("Specify file (-f option), it also has to be run as the first opstion\n"); //change to flags
    break;
	}

	    choice = choose_nmspc(branches, term_array, optarg);
        if (choice == -1) break;
	    namespace_tree **tree =
	      (namespace_tree **) calloc(branches->n, sizeof(void *));

	    tree = namespace_insert(tree, branches[choice]);

	    //                          printf ("%s\n", branches[choice].name);
	    //                          printf ("Number of terms: %d\n", branches[choice].n);
	    printf("#id\t\tlevel\n");
	for (int i = 0; i < branches[choice].n; ++i)
	{
		printf("GO:%.7d\t%5d\n", tree[i]->curr_term->id,
			tree[i]->level);
	}
	//Free memory for chosen namespace's terms
	    freetree(term_array, branches, tree, choice);
	break;
	}
    //If there was an error parsing call arguments, print usage desciption
	default:
	  help();
	break;
	}
    }
  //Free memory for basic structures
  if (opened) {
      freenmspc(term_array, branches);
      return 0;
  }
  help();
  return 1;
}

