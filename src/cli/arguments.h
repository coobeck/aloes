#include <getopt.h>
#include <unistd.h>
#include "../parse/parse.h"
#include "../structs/methods.h"

#ifndef ARGUMENTS_H
#define ARGUMENTS_H

/**
 * @brief Print all available namespaces provided by file in main function.
 * @param[terms_array] Pointer to strcture \ref trmarr containing information on:
 * available namespaces number and names. */
void print_all (trmarr * terms_array);


/**
 * @brief Search for namespace name provided as a call argument in main
 * function.
 * @param[branches] Pointer to list of structures \ref namespace_branch which, 
 * contain information on theirs namespace name, pointer to array of 
 * terms of type \ref term which are members of the namespace and their number.
 * @param[arr] Pointer to structure of type \ref trmarr containing all
 * parsed terms.
 * @param[nmspc] String literal provided in call argument as namespace name/
 */
int choose_nmspc (namespace_branch * branches, trmarr * arr, char *nmspc);

/** @brief Print short description from file ./help.txt */
void help ();

int levels(int argc, char **argv);

#endif
