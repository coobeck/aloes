#include "./help_funcs.h"

#define BUFSIZ 32

/* some string literals to compare against */
const char *id = "id:";
const char *is_a = "is_a:";
const char *nmspcname = "namespace:";
const char *is_obsolete = "is_obsolete: true\n";
const char *name = "name:";

/* Following add* functions depend on file formatting */

    int
add_id(char *line)
    /* Return id of term that's being parsed */
{
    char *buff = strtok(NULL, ":");

    buff = strtok(NULL, "\n");
    int id = atoi(buff);
    return id;
}

    char *
add_names(char *line)
    /* Return name/namespace of term that's being parsed */
{
    char *buff = strtok(NULL, "\n");
    return buff;
}

    int
add_is_a(char *line)
{
    /* Return id of parent of term that's being parsed */
    char *buff = strtok(NULL, ":");

    buff = strtok(NULL, " ");
    if (buff != NULL)
    {
	int is_a = atoi(buff);
	return is_a;
    }
    return -1;
}

    int
get_nmspc(char **nmspc, char *nmspcname, int *count)
{
    /* if namespace of term in question hasnt been added
     * to the pool "nmspc" add it */
    int flag = 1;
    //iterate through all added namespaces
    for (int i = 0; i < *count; ++i)
    {
	if (strcmp(nmspc[i], nmspcname) == 0)
	    flag = 0;
    }
    if (flag)
    {
	++(*count); //increment number of namespaces
	//allocate additional space in array
	nmspc[(*count) - 1] = (char *) malloc(BUFSIZ);
	//pass name of namespace
	strcpy(nmspc[(*count) - 1], nmspcname);
    }
    return *count;
}

    int
cat_cmp(char *line, term *curr_term, char **nmspc, int *no_nmspcs,
	int *no_terms)
    /* compare against input file's beggining of lines
     * under "[Term]" tag, call appropriate add* function */
{
    if (strcmp(line, is_obsolete) == 0)
	//special case for obsolete terms
    {
	//free allocated space
	free(curr_term->nmspc);
	//decrement number of terms
	--(*no_terms);
	return 1;
    }

    char *cat = strtok(line, " ");

    if (strcmp(line, id) == 0)
    {
	curr_term->id = add_id(line);
    //set number of parents to 0
	curr_term->no_parents = 0;
	return 0;
    }
//    if (strcmp(line, name) == 0)
//    {
//	curr_term->name = (char *) malloc(BUFSIZ);
//    //copy parsed name to terms' name
//	strcpy(curr_term->name, add_names(line));
//	return 0;
//    }
    if (strcmp(line, nmspcname) == 0)
    {
	curr_term->nmspc = (char *) malloc(BUFSIZ);
    //copy parsed name to terms' name
	strcpy(curr_term->nmspc, add_names(line));
    //if namespace hasnt been seen before, add it to the pool
	*no_nmspcs = get_nmspc(nmspc, curr_term->nmspc, no_nmspcs);
	return 0;
    }
    if (strcmp(line, is_a) == 0)
    {
	curr_term->no_parents++;
    //expand space for parent
	curr_term->parents =
	    (int *) realloc(curr_term->parents,
		    sizeof(int) * (curr_term->no_parents));
	if (curr_term->parents != NULL)
	{
	    curr_term->parents[(curr_term->no_parents) - 1] = add_is_a(line);
	}
    }
    return 0;
}
