#include <stdlib.h>
#include "../structs/structs.c"

int compare(const void *first, const void *second) {
    term *eval1 = (term *) first;
    term *eval2 = (term *) second;
    return eval1->id - eval2->id;
}
