#ifndef HELP_FUNCS_H
#define HELP_FUNCS_H

#define HELP_FUNCS_H
#include "../structs/structs.c"
#include <string.h>

/** @brief Add parameters to parsed structure \ref term
 * @param[line] Line of input file between "[Term]" tags
 * @param[curr_term] Structure that function adds parameters to.
 * @param[namespaces] Array of string consisting of no or some known
 * namespaces' names from input file/
 * @param[no_nmspcs] Keeps track of how many namespaces are there so far
 * @param[no_terms] Keeps track of how many terms are there so far
 * @return Exit status (may be ommited).
 */
int cat_cmp (char *line, term * curr_term, char **namespaces, int *no_nmspcs,
	     int *no_terms);

#endif
