#define BUFSIZ 256

#include "parse.h"
#include "./sortmod.c"


trmarr *
parse(FILE *stream)
{
  char *line = NULL; //initialize pointer to lines
  size_t buflen = BUFSIZ;
  term *term_array = NULL; //initialize array of terms

  int no_terms = 0; //number of terms

  char **namespaces = NULL; //initialize array of namespaces' names

  namespaces = (char **) malloc(buflen);
  int no_nmspcs = 0; //number of namespaces

  /* parse each line of file */
  while (getline(&line, &buflen, stream) != -1)
    {
      if (strcmp(line, "[Term]\n") == 0) //check if term information began
	{
	  ++no_terms;

      /* expand space for array of terms (a vector of sorts) */
	  term_array = (term *) realloc(term_array, no_terms * (sizeof(term)));
	  memset(&(term_array[no_terms - 1]), 0, sizeof(term)); //init term
      //read lines
	while (getline(&line, &buflen, stream) != -1)
	{
	    //if line is blank -> go back to "[Term]" search
	if (strcmp(line, "\n") == 0)
		break;
	    cat_cmp(line, &term_array[no_terms - 1], namespaces,
		       &no_nmspcs, &no_terms);
	}
	}
    }
  free(line); //free pointer to line

  trmarr *return_arr = (trmarr *) malloc(sizeof(trmarr)); // init return struct

  return_arr->terms = NULL;
  return_arr->terms = term_array; //pass pointer to terms array

  return_arr->no_terms = no_terms;
  //sort terms
  for (int i = 0; i < no_terms - 1; ++i) {
      if (return_arr->terms[i].id > return_arr->terms[i + 1].id) {
  qsort(term_array, no_terms, sizeof(term), & compare);
      }
  }

  return_arr->namespaces = namespaces;
  return_arr->count = no_nmspcs;

  return return_arr;
};
