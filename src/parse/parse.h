#ifndef PARSE_H
#define PARSE_H

#include "help_funcs.h"
#include <stdio.h>

/**
 * @brief Initialises structure containing information on all parsable
 * terms in "stream" file.
 * @param[stream] Name of file to parse.
 * @return Pointer to newly initialised \ref trmarr
 */
trmarr *parse (FILE * stream);

#endif
