var searchData=
[
  ['id_20',['id',['../structterm.html#aa17667d3237b5d860bd45d24bd61fec4',1,'term::id()'],['../help__funcs_8cpp.html#aeffa2f0815ce90fecbda9aac199143db',1,'id():&#160;help_funcs.cpp']]],
  ['initialise_5fbranches_21',['initialise_branches',['../methods_8cpp.html#ab64cd393bc94e66eb80a526a4ef586b2',1,'initialise_branches(trmarr *term_array):&#160;methods.cpp'],['../methods_8h.html#ab64cd393bc94e66eb80a526a4ef586b2',1,'initialise_branches(trmarr *term_array):&#160;methods.cpp']]],
  ['is_5fa_22',['is_a',['../help__funcs_8cpp.html#ace158b65d34ad7f7723a5fdac5faad19',1,'help_funcs.cpp']]],
  ['is_5fobsolete_23',['is_obsolete',['../help__funcs_8cpp.html#a9053daf118dca3c76341ca3a2c81d742',1,'help_funcs.cpp']]]
];
