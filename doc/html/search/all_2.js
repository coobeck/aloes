var searchData=
[
  ['cat_5fcmp_7',['cat_cmp',['../help__funcs_8cpp.html#ae4bc4442dd9610450596a79482e9eacb',1,'cat_cmp(char *line, term *curr_term, char **nmspc, int *no_nmspcs, int *no_terms):&#160;help_funcs.cpp'],['../help__funcs_8h.html#a5d41fe9e9a9ff2237821ddf706f00ead',1,'cat_cmp(char *line, term *curr_term, char **namespaces, int *no_nmspcs, int *no_terms):&#160;help_funcs.cpp']]],
  ['choose_5fnmspc_8',['choose_nmspc',['../arguments_8cpp.html#acff08e8462c259ac7d182475e53f95d0',1,'choose_nmspc(namespace_branch *branches, trmarr *arr, char *nmspc):&#160;arguments.cpp'],['../arguments_8h.html#acff08e8462c259ac7d182475e53f95d0',1,'choose_nmspc(namespace_branch *branches, trmarr *arr, char *nmspc):&#160;arguments.cpp']]],
  ['count_9',['count',['../structtrmarr.html#ab5e4142d68348ce3ed2e24c0a5694fc0',1,'trmarr']]],
  ['curr_5fterm_10',['curr_term',['../structnamespace__tree.html#ac45020330c594c524d32ed1bbd52ec25',1,'namespace_tree']]]
];
