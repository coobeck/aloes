\@ifundefined {etoctocstyle}{\let \etoc@startlocaltoc \@gobble \let \etoc@settocdepth \@gobble \let \etoc@depthtag \@gobble \let \etoc@setlocaltop \@gobble }{}
\contentsline {section}{\numberline {1}Main Page}{1}{section.1}%
\contentsline {section}{\numberline {2}Class Index}{1}{section.2}%
\contentsline {subsection}{\numberline {2.1}Class List}{1}{subsection.2.1}%
\contentsline {section}{\numberline {3}File Index}{1}{section.3}%
\contentsline {subsection}{\numberline {3.1}File List}{1}{subsection.3.1}%
\contentsline {section}{\numberline {4}Class Documentation}{2}{section.4}%
\contentsline {subsection}{\numberline {4.1}namespace\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}branch Struct Reference}{2}{subsection.4.1}%
\contentsline {subsubsection}{\numberline {4.1.1}Detailed Description}{2}{subsubsection.4.1.1}%
\contentsline {subsubsection}{\numberline {4.1.2}Member Data Documentation}{3}{subsubsection.4.1.2}%
\contentsline {paragraph}{\numberline {4.1.2.1}n}{3}{paragraph.4.1.2.1}%
\contentsline {paragraph}{\numberline {4.1.2.2}name}{3}{paragraph.4.1.2.2}%
\contentsline {paragraph}{\numberline {4.1.2.3}terms}{3}{paragraph.4.1.2.3}%
\contentsline {subsection}{\numberline {4.2}namespace\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}tree Struct Reference}{3}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}Detailed Description}{4}{subsubsection.4.2.1}%
\contentsline {subsubsection}{\numberline {4.2.2}Member Data Documentation}{4}{subsubsection.4.2.2}%
\contentsline {paragraph}{\numberline {4.2.2.1}curr\_term}{4}{paragraph.4.2.2.1}%
\contentsline {paragraph}{\numberline {4.2.2.2}level}{4}{paragraph.4.2.2.2}%
\contentsline {paragraph}{\numberline {4.2.2.3}parents}{4}{paragraph.4.2.2.3}%
\contentsline {subsection}{\numberline {4.3}term Struct Reference}{4}{subsection.4.3}%
\contentsline {subsubsection}{\numberline {4.3.1}Detailed Description}{4}{subsubsection.4.3.1}%
\contentsline {subsubsection}{\numberline {4.3.2}Member Data Documentation}{4}{subsubsection.4.3.2}%
\contentsline {paragraph}{\numberline {4.3.2.1}id}{5}{paragraph.4.3.2.1}%
\contentsline {paragraph}{\numberline {4.3.2.2}name}{5}{paragraph.4.3.2.2}%
\contentsline {paragraph}{\numberline {4.3.2.3}nmspc}{5}{paragraph.4.3.2.3}%
\contentsline {paragraph}{\numberline {4.3.2.4}no\_parents}{5}{paragraph.4.3.2.4}%
\contentsline {paragraph}{\numberline {4.3.2.5}parents}{5}{paragraph.4.3.2.5}%
\contentsline {subsection}{\numberline {4.4}trmarr Struct Reference}{5}{subsection.4.4}%
\contentsline {subsubsection}{\numberline {4.4.1}Detailed Description}{6}{subsubsection.4.4.1}%
\contentsline {subsubsection}{\numberline {4.4.2}Member Data Documentation}{6}{subsubsection.4.4.2}%
\contentsline {paragraph}{\numberline {4.4.2.1}count}{6}{paragraph.4.4.2.1}%
\contentsline {paragraph}{\numberline {4.4.2.2}namespaces}{6}{paragraph.4.4.2.2}%
\contentsline {paragraph}{\numberline {4.4.2.3}no\_terms}{6}{paragraph.4.4.2.3}%
\contentsline {paragraph}{\numberline {4.4.2.4}terms}{6}{paragraph.4.4.2.4}%
\contentsline {section}{\numberline {5}File Documentation}{6}{section.5}%
\contentsline {subsection}{\numberline {5.1}R\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}E\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}A\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}D\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}M\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}E.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}md File Reference}{6}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}src/cli/arguments.cpp File Reference}{6}{subsection.5.2}%
\contentsline {subsubsection}{\numberline {5.2.1}Function Documentation}{7}{subsubsection.5.2.1}%
\contentsline {paragraph}{\numberline {5.2.1.1}choose\_nmspc()}{7}{paragraph.5.2.1.1}%
\contentsline {paragraph}{\numberline {5.2.1.2}help()}{8}{paragraph.5.2.1.2}%
\contentsline {paragraph}{\numberline {5.2.1.3}print\_all()}{8}{paragraph.5.2.1.3}%
\contentsline {subsection}{\numberline {5.3}src/cli/arguments.h File Reference}{9}{subsection.5.3}%
\contentsline {subsubsection}{\numberline {5.3.1}Function Documentation}{9}{subsubsection.5.3.1}%
\contentsline {paragraph}{\numberline {5.3.1.1}choose\_nmspc()}{10}{paragraph.5.3.1.1}%
\contentsline {paragraph}{\numberline {5.3.1.2}help()}{10}{paragraph.5.3.1.2}%
\contentsline {paragraph}{\numberline {5.3.1.3}print\_all()}{10}{paragraph.5.3.1.3}%
\contentsline {subsection}{\numberline {5.4}src/cli/help.txt File Reference}{11}{subsection.5.4}%
\contentsline {subsection}{\numberline {5.5}src/main.cpp File Reference}{11}{subsection.5.5}%
\contentsline {subsubsection}{\numberline {5.5.1}Function Documentation}{12}{subsubsection.5.5.1}%
\contentsline {paragraph}{\numberline {5.5.1.1}main()}{12}{paragraph.5.5.1.1}%
\contentsline {subsection}{\numberline {5.6}src/parse/help\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}funcs.cpp File Reference}{12}{subsection.5.6}%
\contentsline {subsubsection}{\numberline {5.6.1}Macro Definition Documentation}{13}{subsubsection.5.6.1}%
\contentsline {paragraph}{\numberline {5.6.1.1}BUFSIZ}{14}{paragraph.5.6.1.1}%
\contentsline {subsubsection}{\numberline {5.6.2}Function Documentation}{14}{subsubsection.5.6.2}%
\contentsline {paragraph}{\numberline {5.6.2.1}add\_id()}{14}{paragraph.5.6.2.1}%
\contentsline {paragraph}{\numberline {5.6.2.2}add\_is\_a()}{14}{paragraph.5.6.2.2}%
\contentsline {paragraph}{\numberline {5.6.2.3}add\_names()}{14}{paragraph.5.6.2.3}%
\contentsline {paragraph}{\numberline {5.6.2.4}cat\_cmp()}{14}{paragraph.5.6.2.4}%
\contentsline {paragraph}{\numberline {5.6.2.5}get\_nmspc()}{15}{paragraph.5.6.2.5}%
\contentsline {subsubsection}{\numberline {5.6.3}Variable Documentation}{15}{subsubsection.5.6.3}%
\contentsline {paragraph}{\numberline {5.6.3.1}id}{15}{paragraph.5.6.3.1}%
\contentsline {paragraph}{\numberline {5.6.3.2}is\_a}{15}{paragraph.5.6.3.2}%
\contentsline {paragraph}{\numberline {5.6.3.3}is\_obsolete}{15}{paragraph.5.6.3.3}%
\contentsline {paragraph}{\numberline {5.6.3.4}name}{16}{paragraph.5.6.3.4}%
\contentsline {paragraph}{\numberline {5.6.3.5}nmspcname}{16}{paragraph.5.6.3.5}%
\contentsline {subsection}{\numberline {5.7}src/parse/help\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}funcs.h File Reference}{16}{subsection.5.7}%
\contentsline {subsubsection}{\numberline {5.7.1}Macro Definition Documentation}{17}{subsubsection.5.7.1}%
\contentsline {paragraph}{\numberline {5.7.1.1}HELP\_FUNCS\_H}{17}{paragraph.5.7.1.1}%
\contentsline {subsubsection}{\numberline {5.7.2}Function Documentation}{17}{subsubsection.5.7.2}%
\contentsline {paragraph}{\numberline {5.7.2.1}cat\_cmp()}{17}{paragraph.5.7.2.1}%
\contentsline {subsection}{\numberline {5.8}src/parse/parse.cpp File Reference}{18}{subsection.5.8}%
\contentsline {subsubsection}{\numberline {5.8.1}Function Documentation}{18}{subsubsection.5.8.1}%
\contentsline {paragraph}{\numberline {5.8.1.1}parse()}{19}{paragraph.5.8.1.1}%
\contentsline {subsection}{\numberline {5.9}src/parse/parse.h File Reference}{19}{subsection.5.9}%
\contentsline {subsubsection}{\numberline {5.9.1}Function Documentation}{20}{subsubsection.5.9.1}%
\contentsline {paragraph}{\numberline {5.9.1.1}parse()}{20}{paragraph.5.9.1.1}%
\contentsline {subsection}{\numberline {5.10}src/structs/methods.cpp File Reference}{21}{subsection.5.10}%
\contentsline {subsubsection}{\numberline {5.10.1}Function Documentation}{22}{subsubsection.5.10.1}%
\contentsline {paragraph}{\numberline {5.10.1.1}bin\_find()}{22}{paragraph.5.10.1.1}%
\contentsline {paragraph}{\numberline {5.10.1.2}find\_parents()}{23}{paragraph.5.10.1.2}%
\contentsline {paragraph}{\numberline {5.10.1.3}freenmspc()}{23}{paragraph.5.10.1.3}%
\contentsline {paragraph}{\numberline {5.10.1.4}freetree()}{24}{paragraph.5.10.1.4}%
\contentsline {paragraph}{\numberline {5.10.1.5}initialise\_branches()}{24}{paragraph.5.10.1.5}%
\contentsline {paragraph}{\numberline {5.10.1.6}level()}{25}{paragraph.5.10.1.6}%
\contentsline {paragraph}{\numberline {5.10.1.7}namespace\_insert()}{25}{paragraph.5.10.1.7}%
\contentsline {subsection}{\numberline {5.11}src/structs/methods.h File Reference}{26}{subsection.5.11}%
\contentsline {subsubsection}{\numberline {5.11.1}Function Documentation}{27}{subsubsection.5.11.1}%
\contentsline {paragraph}{\numberline {5.11.1.1}freenmspc()}{27}{paragraph.5.11.1.1}%
\contentsline {paragraph}{\numberline {5.11.1.2}freetree()}{28}{paragraph.5.11.1.2}%
\contentsline {paragraph}{\numberline {5.11.1.3}initialise\_branches()}{28}{paragraph.5.11.1.3}%
\contentsline {paragraph}{\numberline {5.11.1.4}namespace\_insert()}{29}{paragraph.5.11.1.4}%
\contentsline {subsection}{\numberline {5.12}src/structs/structs.cpp File Reference}{30}{subsection.5.12}%
\contentsline {subsubsection}{\numberline {5.12.1}Macro Definition Documentation}{31}{subsubsection.5.12.1}%
\contentsline {paragraph}{\numberline {5.12.1.1}STRUCTS\_H}{31}{paragraph.5.12.1.1}%
\contentsline {subsubsection}{\numberline {5.12.2}Typedef Documentation}{31}{subsubsection.5.12.2}%
\contentsline {paragraph}{\numberline {5.12.2.1}namespace\_branch}{31}{paragraph.5.12.2.1}%
\contentsline {paragraph}{\numberline {5.12.2.2}namespace\_tree}{31}{paragraph.5.12.2.2}%
\contentsline {paragraph}{\numberline {5.12.2.3}term}{31}{paragraph.5.12.2.3}%
\contentsline {paragraph}{\numberline {5.12.2.4}trmarr}{31}{paragraph.5.12.2.4}%
\contentsline {section}{Index}{33}{section*.68}%
